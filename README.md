# FAQs

Answers to your burning pyiron questions. 

### How to use the repo?

#### Add an issue

See [here](https://gitlab.mpcdf.mpg.de/pyiron/faqs/-/issues/1)

#### Search for an issue

Go to issues, in search bar add a query. You can filter through queries by adding labels.
